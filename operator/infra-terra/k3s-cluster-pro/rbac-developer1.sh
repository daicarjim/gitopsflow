#!/bin/bash

#The developer follow the steps for authorization and authentication to cluster.
#change the variable YOURNAME in each commando for your nick or name then save this file witn :wq and execute:

#1) chmod +x rbac-developer1.sh
#2) ./rbac-developer1.sh

openssl genrsa -out YOURNAME.key 4096


cat <<EOF > csr.cnf
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn
[ dn ]
CN = YOURNAME
O = dev
[ v3_ext ]
authorityKeyIdentifier=keyid,issuer:always
basicConstraints=CA:FALSE
keyUsage=keyEncipherment,dataEncipherment
extendedKeyUsage=serverAuth,clientAuth
EOF

openssl req -config ./csr.cnf -new -key YOURNAME.key -nodes -out YOURNAME.csr

#After the developer send file .csr to Operator of Cluster
