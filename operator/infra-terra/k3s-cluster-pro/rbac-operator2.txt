#Then after of receive .csr of developer follow this steps in your cluster 
#Create for sign certificates with CA (csr.yaml) for RBAC
#NOTE:Replace YOURNAME for nick o name of the developer.

#Create file
vim csr.yaml
#Copy-paste
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: mycsr
spec:
  groups:
  - system:authenticated
  request: ${BASE64_CSR}
  usages:
  - digital signature
  - key encipherment
  - server auth
  - client auth


#STEPS WITH EXAMPLE
# Encoding the .csr file in base64
#1) export BASE64_CSR=$(cat ./YOURNAME.csr | base64 | tr -d '\n')
# Substitution of the BASE64_CSR env variable and creation of the CertificateSigninRequest resource
#2) cat csr.yaml | envsubst | kubectl apply -f -
# Checking the status of the newly created CSR
#3) kubectl get csr
#4) kubectl certificate approve mycsr
#5) kubectl get csr
#Create crt
#6) kubectl get csr mycsr -o jsonpath='{.status.certificate}' | base64 --decode > YOURNAME.crt
#7) openssl x509 -in ./YOURNAME.crt -noout -text

#Create template kubeconfig.tpl for RBAC

#Create file
vim kubeconfig.tpl
#Copy-paste
apiVersion: v1
kind: Config
clusters:
- cluster:
    certificate-authority-data: ${CLUSTER_CA}
    server: ${CLUSTER_ENDPOINT}
  name: ${CLUSTER_NAME}
users:
- name: ${USER}
  user:
    client-certificate-data: ${CLIENT_CERTIFICATE_DATA}
contexts:
- context:
    cluster: ${CLUSTER_NAME}
    user: YOURNAME
  name: ${USER}-${CLUSTER_NAME}
current-context: ${USER}-${CLUSTER_NAME}



#Create kubeconfig with VARIABLES

# User identifier
#1) export USER="YOURNAME"
# Cluster Name (get it from the current context)
#2) export CLUSTER_NAME=$(kubectl config view --minify -o jsonpath={.current-context})
# Client certificate
#3) export CLIENT_CERTIFICATE_DATA=$(kubectl get csr mycsr -o jsonpath='{.status.certificate}')
# Cluster Certificate Authority
#4) export CLUSTER_CA=$(kubectl config view --raw -o json | jq -r '.clusters[] | select(.name == "'$(kubectl config current-context)'") | .cluster."certificate-authority-data"')
# API Server endpoint
#5) export CLUSTER_ENDPOINT=$(kubectl config view --raw -o json | jq -r '.clusters[] | select(.name == "'$(kubectl config current-context)'") | .cluster."server"')
#and substitute them using, once again, the convenient envsubst utility:
#6) cat kubeconfig.tpl | envsubst > kubeconfig

#Copy file kubeconfig and CHANGE address IP to IP Master Cluster.
#Then after send .crt and kubeconfig (not .tpl) to developer for configuration and complete authentication.
