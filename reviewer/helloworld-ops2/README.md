# Ingress production k8s with loadbalancer in Hetzner-Cloud

If you provider is Hetzner-Cloud and you want loadbalancer production for your cluster k8s (k3s) then this is your tutorial. 


## How to use

## Requirements 📋


The next tutorial you should apply before:
```
https://gitlab.com/daicarjim/k3s-cluster-pro
```
 
Or you should have a cluster basic in hetzner cloud with LoadBalancer.


## Clone this repository 📄

```

git clone https://gitlab.com/daicarjim/ingress-production-hetzner-with-loadbalancerk8s.git
```
_WARNING: You should download this repository in you master machine of cluster because you have that apply manifests yaml._


## Steps Ingress production Kubernetes🔧

1) Register domain in freenom.com (you can create domain free for three months, terminated in .tk = example diatonica-comercial.tk).
2) Access to Hetzner DNS and scanner domain "diatonica-comercial.tk" (and NameServers freenom)


![alt text](freenom-nameservers.png)


3) Create records dns in Hetzner Cloud 

```
TYPE  NAME   VALUE

A     @      IPLOADBALANCER

A     WWW    IPLOADBALANCER
```

![alt text](dns-hetzner.png)


4) Access to Hetzner Cloud and add service to LoadBalancer with rules:


![alt text](hetzner load balancer configuration.png)


NOTES:

*Add service TLS and create certificate, then add to LoadBalancer

*In the option redirect http-redirect(301) mark OK.

# INSTALL AND CONFIGURATION YAMLS IN YOUR MACHINE MASTER ⚙️

1) Create Namespace,Configmap and ServiceAccount for the Ingress

```
kubectl apply -f 001-create-ns_config_sa.yaml
```

2) Create Ingress Controller NGINX but first change the line 171 for your domain (example=diatonica-comercial.tk) then:

```
kubectl apply -f 002-create-ingress-controller-change_line-171.yaml
```

3) Create a service default backend. 

```
kubectl apply -f 003-create-backend-service.yaml
```

Wait a 5 minutes and you can see the page in the browser:


![alt text](domain production.png)


4) You can change the rules with the examples 004 and 005. In the example 004 you can modified a subdomain.


![alt text](configuration subdomain yaml.png)


Save the changes and apply:

```
kubectl apply -f 004-create-sampleapp1-change_line-50.yaml

```

Remember if you want configure rules ingress in yamls for subdomain, then you should add these subdomains to hetzner DNS records. Also you should wait a little minutes a while actualize rules in load balancer.


![alt text](example dns subdomain hetzner.png)


Then in the browser you will see:


![alt text](subdomain production.png)


5) With the example 005 you can modified to path rule: 


![alt text](configuration path yaml.png)


Save the changes and apply:

```
kubectl apply -f 005-create-sampleapp2-change_line_50.yaml

```

Then in the browser you will see:


![alt text](path serviceb.png)


# CONGRATULATION... Now you have an ingress in mode-production with Hetzner-Cloud and Load Balancer.


# GRATITUDE 🎁

```
https://github.com/srcmkr/csharpdevops
https://www.youtube.com/watch?v=92YvP51R5Vw
https://www.udemy.com/course/kubernetes-de-principiante-a-experto/

```

⌨️ Esto se hizo con el ❤️ para la comunidad por [daicarjim](https://gitlab.com/daicarjim) 👍
